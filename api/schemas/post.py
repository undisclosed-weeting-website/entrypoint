from typing import Optional

from pydantic import BaseModel


class PostBase(BaseModel):
    title: Optional[str] = ""
    description: Optional[str] = ""


class PostCreate(PostBase):
    title: str


class Post(PostBase):
    id: int
    title: str
