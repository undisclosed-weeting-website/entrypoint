import grpc

import protos.post.post_pb2 as pb2
import protos.post.post_pb2_grpc as pb2_grpc


class PostClient:
    """
    Client for gRPC functionality
    """

    def __init__(self):
        self.host = "post"
        self.server_port = 50051

        self.channel = grpc.insecure_channel("{}:{}".format(self.host, self.server_port))
        self.stub = pb2_grpc.PostStub(self.channel)

    def create_post(self, title, description):
        message = pb2.CreatePostMessage(title=title, description=description)
        return self.stub.CreatePost(message)

    def retrieve_post(self):
        message = pb2.RetrievePostsMessage()
        return self.stub.RetrievePosts(message)


post = PostClient()
