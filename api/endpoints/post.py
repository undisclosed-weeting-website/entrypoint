from fastapi import APIRouter
from google.protobuf.json_format import MessageToDict

from api import schemas

from ..grpc_connections.post import post

router = APIRouter()


@router.get("/")
def get_posts():
    result = post.retrieve_post()
    return MessageToDict(result)


@router.post("/", response_model=schemas.Post)
def post_post(post_in: schemas.PostCreate):
    result = post.create_post(**post_in.__dict__)
    return MessageToDict(result)
